<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template match="/">
        <xsl:apply-templates select="Recipes/Recipe"/>
    </xsl:template>

    <xsl:template match="Recipe">
        <xsl:for-each select=".">
            <xsl:value-of select="Title" />
            <xsl:text>&#xa;</xsl:text>
            <xsl:apply-templates select="Ingredients"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="Ingredients">
        <xsl:for-each select="Ingredient">
            <xsl:value-of select="@name" /><xsl:text>, </xsl:text>
            <xsl:value-of select="@qty" /><xsl:text>, </xsl:text>
            <xsl:value-of select="@unit" />
            <xsl:text>&#xa;</xsl:text>
        </xsl:for-each>
    </xsl:template>

</xsl:stylesheet>