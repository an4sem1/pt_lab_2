<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">

    <xsl:template match="/">
        <html>
            <body>
                <xsl:apply-templates select="Recipes/Recipe"/>
            </body>
        </html>
    </xsl:template>

    <xsl:template match="Recipe">
        <xsl:for-each select=".">
            <li><xsl:value-of select="Title" /></li>
            <xsl:text>&#xa;</xsl:text>
            <xsl:apply-templates select="Nutrients"/>
        </xsl:for-each>
    </xsl:template>

    <xsl:template match="Nutrients">
        <xsl:choose>
            <xsl:when test="number(@calories) > 300">
                <p style="background-color: red;">Calorii: <xsl:value-of select="@calories"/></p>
            </xsl:when>

            <xsl:when test="number(@calories) > 200 and number(@calories) &lt;= 300">
                <p style="background-color: yellow;">Calorii: <xsl:value-of select="@calories"/></p>
            </xsl:when>

            <xsl:when test="number(@calories) > 100 and number(@calories) &lt;= 200">
                <p style="background-color: green;">Calorii: <xsl:value-of select="@calories"/></p>
            </xsl:when>

            <xsl:otherwise>
                Calorii: <xsl:value-of select="@calories"/>
            </xsl:otherwise>

        </xsl:choose>
    </xsl:template>

</xsl:stylesheet>