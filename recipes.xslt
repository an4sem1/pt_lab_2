<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="https://www.w3.org/1999/XSL/Format" >
    <xsl:output method="xml" indent="yes" />
    <xsl:template match="/">
        <fo:root xmlns:fo="https://www.w3.org/1999/XSL/Format">
            <fo:layout-master-set>
                <fo:simple-page-master master-name="A4" page-width="21cm" page-height="29.7cm">
                    <fo:region-body />
                </fo:simple-page-master>
            </fo:layout-master-set>
            <fo:page-sequence master-reference="A4">
                Recipes:
                <fo:flow flow-name="xsl-region-body">
                    <xsl:for-each select="Recipes/Recipe">
                        <fo:block font-size="14pt">
                            <xsl:apply-templates select="." />
                        </fo:block>
                    </xsl:for-each>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>
    <xsl:template match="Recipe">
        <xsl:value-of select="Title">

        </xsl:value-of>
    </xsl:template>
</xsl:stylesheet>