package jdom2;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;
import org.jdom2.xpath.*;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;

public class JDOM2_XPath {
    public static void main(String[] args) {
        SAXBuilder sb = new SAXBuilder(XMLReaders.XSDVALIDATING);
        File xmlFile = new File("recipes_xsd.xml");


        try {
            Document jdoc = sb.build(xmlFile); //PARSE
            ex1(jdoc);
            ex2(jdoc);


        } catch (JDOMException | IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }


    private static void ex1( Document jdoc){
        XPathFactory xFactory = XPathFactory.instance();
        String expreStr = "/child::Recipes/child::Recipe"; //(RN)->(REN-Recipes)->(D, R1, R2, R3)->(R1, R2, R3)
        XPathExpression<Object> expr = xFactory.compile(expreStr);
        List<Object> recipeList = expr.evaluate(jdoc);
        Iterator it = recipeList.iterator();
        int i = 0;
        while(it.hasNext()){
            i++;
            Element recipe = (Element)it.next();
            System.out.println("Reteta" +i+": "+ recipe.getChildText("Title"));
            XPathExpression<Object> expr2 = xFactory.compile(".//Ingredient/@name"); //(I1, I2...)->(n1 n2..)
            List<Object> ingrList = expr2.evaluate(recipe);
            Iterator iter2 = ingrList.iterator();
            int a = 0;
            while(iter2.hasNext()){
                a++;
                Attribute ingredient = (Attribute) iter2.next();
                System.out.println("\tIngredient" +a+": "+ ingredient.getValue());
            }
        }
    }

    private static void ex2( Document jdoc){
        XPathFactory xFactory = XPathFactory.instance();
        String expreStr = "/Recipes/Recipe[last()-1]/Ingredients/Ingredient[position() = 2]/@name"; //(RN)->(REN-Recipes)->(D, R1, R2, R3)->(R1, R2, R3)
        XPathExpression<Object> expr = xFactory.compile(expreStr);
        Object alal = expr.evaluate(jdoc);
        Attribute a = (Attribute)alal;
        System.out.println(alal);

    }

    private static void showRecipesWithIngredients(Element rootNode) {








        //        for (var recipe : rootNode.getChildren("Recipe")) {
//            System.out.println("Rețetă: " + recipe.getChildText("Title"));
//            System.out.println("Listă de ingrediente: ");
//
//            for (var ingredient : recipe.getChild("Ingredients").getChildren("Ingredient")) {
//                System.out.println("- " + ingredient.getAttributeValue("qty")
//                        + " " + ingredient.getAttributeValue("unit")
//                        + " " + ingredient.getAttributeValue("name"));
//            }
//
//            System.out.println();
//        }
    }
}
