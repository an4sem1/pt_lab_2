package jdom2;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

import java.io.File;
import java.io.IOException;
import java.util.List;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamSource;

import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import org.jdom2.transform.*;

public class JDOM2_XSLT {
    public static void main(String[] args) {
        SAXBuilder sb = new SAXBuilder(XMLReaders.XSDVALIDATING);
        File xmlDoc = new File("recipes_xsd.xml");

        try {

            Document jdomDoc = sb.build(xmlDoc);
            String xsltString = "recipes.xslt";
            StreamSource xslt = new StreamSource(xsltString);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer(xslt);
            JDOMSource in = new JDOMSource(jdomDoc);
            JDOMResult out = new JDOMResult();
            transformer.transform(in, out);
            List result = out.getResult();
            XMLOutputter xmlOutputter = new XMLOutputter();
            xmlOutputter.setFormat(Format.getPrettyFormat());
            xmlOutputter.output(result, System.out);

        } catch (JDOMException | IOException | TransformerException e) {
            e.printStackTrace();
        }
    }
}
