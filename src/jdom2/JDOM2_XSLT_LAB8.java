package jdom2;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;
import org.jdom2.output.Format;
import org.jdom2.output.XMLOutputter;
import javax.xml.transform.*;
import javax.xml.transform.stream.StreamSource;

import org.jdom2.transform.*;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class JDOM2_XSLT_LAB8 {

    public static void main(String[] args) {
        SAXBuilder sb = new SAXBuilder(XMLReaders.XSDVALIDATING);
        File xmlFile = new File("Recipes_xsd_Claudiu.xml");
        try {
            Document jdomDoc = sb.build(xmlFile);
            Element rootNode = jdomDoc.getRootElement();
            System.out.println("Root node is: " + rootNode.getName());

            String xsltStr = "Recipes4.xslt";
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            StreamSource xslt = new StreamSource(xsltStr);
            Transformer tr = transformerFactory.newTransformer(xslt);
            JDOMSource in = new JDOMSource(jdomDoc);
            JDOMResult out = new JDOMResult();
            tr.transform(in, out);

            List result = out.getResult();
            XMLOutputter xmlOutputter = new XMLOutputter();
            xmlOutputter.setFormat(Format.getPrettyFormat());
            xmlOutputter.output(result, new FileWriter("Recipes4.html"));
            xmlOutputter.output(result, System.out);
        } catch (JDOMException | IOException jdomex) {
            System.out.println(jdomex.getMessage() + "nu merge");
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }
}
