package jdom2;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.output.XMLOutputter;

import java.io.FileOutputStream;
import java.io.IOException;

public class JDOMDocumentCreate {

    private static Document createDocument() {
        return new Document();
    }

    private static Element createIngredient(Attribute name, Attribute qty, Attribute unit) {
        Element ingredient = new Element("ingredient");

        ingredient.setAttribute(name);
        ingredient.setAttribute(qty);
        ingredient.setAttribute(unit);

        return ingredient;
    }

    private static Element createNutrient(Attribute proteins, Attribute sugars, Attribute fat, Attribute vitamins, Attribute calories) {
        Element nutrients = new Element("nutrients");

        nutrients.setAttribute(proteins);
        nutrients.setAttribute(sugars);
        nutrients.setAttribute(fat);
        nutrients.setAttribute(vitamins);
        nutrients.setAttribute(calories);

        return nutrients;
    }

    private static Element createRecipe() {
        Element recipe = new Element("recipe");

        recipe.setText("recipe");

        return recipe;
    }

    private static void createRecipesXML() {
        Document doc = createDocument();

        Element recipes = new Element("recipes");

        Element description = new Element("description");
        description.setText("This is not a recipe collection");

        Element recipe = createRecipe();

        recipe.addContent(description);
        recipe.addContent(recipe);
        doc.setRootElement(recipes);

        XMLOutputter xmlOutputter = new XMLOutputter();

        try {
            xmlOutputter.output(doc, new FileOutputStream("recipes.xml"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Document doc = createDocument();

        Element catalog = new Element("catalog");

        Element book = new Element("book");
        book.setAttribute(new Attribute("isbn", "123"));

        Element title = new Element("title");
        title.setText("Beginning XML ...");
        book.addContent(title);

        Element author = new Element("author");
        author.setText("Big John");
        book.addContent(author);

        Element numberOfPages = new Element("pages");
        numberOfPages.setText("256");
        book.addContent(numberOfPages);

        catalog.addContent(book);
        doc.setRootElement(catalog);
        //completarea cu continut a radacinii

        XMLOutputter xmlOutputter = new XMLOutputter();

        try {
            xmlOutputter.output(doc, new FileOutputStream("demo.xml"));
        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

}