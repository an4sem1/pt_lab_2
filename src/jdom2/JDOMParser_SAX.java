package jdom2;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

import java.io.File;
import java.io.IOException;

public class JDOMParser_SAX {
    public static void main(String[] args) {
        SAXBuilder sb = new SAXBuilder(XMLReaders.DTDVALIDATING);
        File xmlFile = new File("recipes_ext.xml");


        try {
            Document jdoc = sb.build(xmlFile); //PARSE
            Element rootNode = jdoc.getRootElement();
            System.out.println("Root node us: " + rootNode.getName());
        } catch (JDOMException | IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }
}
