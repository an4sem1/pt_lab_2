package jdom2;

import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;

import java.io.File;
import java.io.IOException;

public class JDOMValidation_XSD {
    public static void main(String[] args) {
        SAXBuilder sb = new SAXBuilder(XMLReaders.XSDVALIDATING);
        File xmlFile = new File("recipes_xsd.xml");

        try {
            Document jdoc = sb.build(xmlFile); //PARSE
            Element rootNode = jdoc.getRootElement();

            showRecipesWithIngredients(rootNode);

        } catch (JDOMException | IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
    }

    private static void showRecipesWithIngredients(Element rootNode) {
        for (var recipe : rootNode.getChildren("Recipe")) {
            System.out.println("Rețetă: " + recipe.getChildText("Title"));
            System.out.println("Listă de ingrediente: ");

            for (var ingredient : recipe.getChild("Ingredients").getChildren("Ingredient")) {
                System.out.println("- " + ingredient.getAttributeValue("qty")
                        + " " + ingredient.getAttributeValue("unit")
                        + " " + ingredient.getAttributeValue("name"));
            }

            System.out.println();
        }
    }
}
