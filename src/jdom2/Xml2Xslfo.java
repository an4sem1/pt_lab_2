package jdom2;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;
import org.jdom2.input.sax.XMLReaders;
import org.jdom2.transform.JDOMSource;

import javax.xml.transform.*;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class Xml2Xslfo {
    public static void main(String[] args) {
        try {
            File xmlFile = new File("recipes_xsd.xml");
            File xsltFile = new File("recipes.xslt");
            File toFile = new File("recipes.fo");

            SAXBuilder sb = new SAXBuilder(XMLReaders.XSDVALIDATING);
            Document jdomDoc = sb.build(xmlFile);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer tr = transformerFactory.newTransformer(new StreamSource(xsltFile));

            JDOMSource src = new JDOMSource(jdomDoc);
            OutputStream out = new FileOutputStream(toFile);
            Result res = new StreamResult(out);

            tr.transform(src, res);

            out.close();
        } catch (JDOMException | IOException | TransformerException e) {
            e.printStackTrace();
        }
    }
}
